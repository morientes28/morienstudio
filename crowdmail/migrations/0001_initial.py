# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Segment'
        db.create_table(u'crowdmail_segment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'crowdmail', ['Segment'])

        # Adding model 'Person'
        db.create_table(u'crowdmail_person', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(default='', max_length=32, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(default='', max_length=32, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=64)),
            ('gender', self.gf('django.db.models.fields.CharField')(default='M', max_length=1)),
            ('solution', self.gf('django.db.models.fields.CharField')(default='', max_length=32, blank=True)),
            ('flag', self.gf('django.db.models.fields.CharField')(default='', max_length=32, blank=True)),
            ('segment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crowdmail.Segment'])),
        ))
        db.send_create_signal(u'crowdmail', ['Person'])

        # Adding model 'Campaign'
        db.create_table(u'crowdmail_campaign', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=12)),
            ('release_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 6, 5, 0, 0))),
            ('identify_flag', self.gf('django.db.models.fields.CharField')(default='', max_length=32, blank=True)),
            ('data_html', self.gf('django.db.models.fields.TextField')(max_length=2048)),
            ('data_title', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'crowdmail', ['Campaign'])

        # Adding model 'MembershipOfCompaign'
        db.create_table(u'crowdmail_membershipofcompaign', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crowdmail.Campaign'])),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crowdmail.Person'])),
        ))
        db.send_create_signal(u'crowdmail', ['MembershipOfCompaign'])


    def backwards(self, orm):
        # Deleting model 'Segment'
        db.delete_table(u'crowdmail_segment')

        # Deleting model 'Person'
        db.delete_table(u'crowdmail_person')

        # Deleting model 'Campaign'
        db.delete_table(u'crowdmail_campaign')

        # Deleting model 'MembershipOfCompaign'
        db.delete_table(u'crowdmail_membershipofcompaign')


    models = {
        u'crowdmail.campaign': {
            'Meta': {'object_name': 'Campaign'},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 6, 5, 0, 0)'}),
            'data_html': ('django.db.models.fields.TextField', [], {'max_length': '2048'}),
            'data_title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identify_flag': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'release_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '12'})
        },
        u'crowdmail.membershipofcompaign': {
            'Meta': {'object_name': 'MembershipOfCompaign'},
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crowdmail.Campaign']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crowdmail.Person']"})
        },
        u'crowdmail.person': {
            'Meta': {'object_name': 'Person'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '64'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'}),
            'flag': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'}),
            'segment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crowdmail.Segment']"}),
            'solution': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'blank': 'True'})
        },
        u'crowdmail.segment': {
            'Meta': {'object_name': 'Segment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['crowdmail']