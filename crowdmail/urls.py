from django.conf.urls import patterns, include, url
from crowdmail.views import IndexView, MembershipView
from crowdmail.views import PersonListView, PersonCreate, PersonUpdate, PersonDelete, PersonAcceptDelete
from crowdmail.views import CampaignListView, CampaignDetailView, CampaignCreate, CampaignLunchView
from crowdmail.views import CampaignUpdate, CampaignDelete, CampaignAcceptDelete


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view()),
    url(r'kontakty/$', PersonListView.as_view(), name='person_list'),
    url(r'kontakty/add/$', PersonCreate.as_view(), name='person_add'),
    url(r'kontakty/(?P<pk>[0-9]+)/edit/$', PersonUpdate.as_view(), name='person_update'),
    url(r'kontakty/(?P<pk>[0-9]+)/delete/$', PersonDelete.as_view(), name='person_delete'),
    url(r'kontakty/(?P<pk>[0-9]+)/deleteAccept/$', PersonAcceptDelete.as_view(), name='person_accept_delete'),

    url(r'kampan/$', CampaignListView.as_view(), name='campaign_list'),
    url(r'kampan/(?P<pk>[0-9]+)/$', CampaignDetailView.as_view(), name='campaign_detail'),
    url(r'kampan/add/$', CampaignCreate.as_view(), name='campaign_add'),
    url(r'kampan/(?P<pk>[0-9]+)/edit/$', CampaignUpdate.as_view(), name='campaign_update'),
    url(r'kampan/(?P<pk>[0-9]+)/delete/$', CampaignDelete.as_view(), name='campaign_delete'),
    url(r'kampan/(?P<pk>[0-9]+)/deleteAccept/$', CampaignAcceptDelete.as_view(), name='campaign_accept_delete'),
    url(r'kampan/(?P<pk>[0-9]+)/lunch/$', CampaignLunchView.as_view(), name='campaign_lunch'),

    url(r'membership/(?P<pk>[0-9]+)/$', MembershipView.as_view(), name="membership_index"),
)
