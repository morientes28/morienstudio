from django.contrib import admin
from crowdmail.models import Person, Campaign, Segment, MembershipOfCompaign

admin.site.register(Person)
admin.site.register(Campaign)
admin.site.register(Segment)
admin.site.register(MembershipOfCompaign)
