from django.http import HttpResponse, Http404
from django.shortcuts import redirect, render
from django.views.generic import TemplateView, ListView, DetailView, DeleteView, CreateView, UpdateView, View
from django.views.generic.edit import BaseDeleteView
from crowdmail.models import Person, Campaign, MembershipOfCompaign, PersonForm, MembershipOfCompaignForm
from datetime import datetime
from sender import Sender
import time

class IndexView(TemplateView):
    template_name = "crowdmail_index.html"


class PersonListView(ListView):
    model = Person
    template_name = "person/person_list.html"

    def head(self, *args, **kwargs):
        last_person = self.get_queryset().latest('id')
        response = HttpResponse('')
        return response


class PersonCreate(CreateView):
    model = Person
    fields = ['name']
    success_url = 'person_list'
    template_name = "person/person_form.html"

    def form_valid(self, form):      
        self.object = form.save()  
        return redirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data(**kwargs)
        context['form_title'] = 'Vytvorenie kontaktu'
        return context

class PersonUpdate(UpdateView):
    model = Person
    template_name = "person/person_form.html"
    success_url = 'person_list'

    def get_form(self, form_class):
        form = super(PersonUpdate, self).get_form(form_class)
        return form

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['form_title'] = 'Editacia kontaktu'
        return context  

    def form_valid(self, form):
        self.object = form.save()    
        return redirect(self.success_url) 

class PersonDelete(DeleteView):
    model = Person
    template_name = "person/person_confirm_delete.html"
    
    def get_object(self, queryset=None):
        obj = super(DeleteView, self).get_object()
        if not obj:
            raise Http404
        return obj        


class PersonAcceptDelete(BaseDeleteView):
    model = Person
    success_url = 'person_list'

    def get(self, request, *args, **kwargs):
        self.get_object().delete()
        return redirect(self.success_url)


class CampaignListView(ListView):
    model = Campaign
    template_name = "campaign/campaign_list.html"

    def head(self, *args, **kwargs):
        response = HttpResponse('')
        return response


class CampaignDetailView(DetailView):
    model = Campaign
    template_name = "campaign/campaign_detail.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CampaignDetailView, self).get_context_data(**kwargs)
        campaign = self.get_object()
        memberships = MembershipOfCompaign.objects.filter(campaign=campaign)
        context['memberships'] = memberships
        return context


class CampaignCreate(CreateView):
    model = Campaign
    fields = ['name']
    success_url = 'campaign_list'
    template_name = "campaign/campaign_form.html"

    def form_valid(self, form):      
        self.object = form.save()
        return redirect(self.success_url)    

    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data(**kwargs)
        context['form_title'] = 'Vytvorenie kampane'
        return context


class CampaignUpdate(UpdateView):
    model = Campaign
    template_name = "campaign/campaign_form.html"
    success_url = 'campaign_list'

    def get_form(self, form_class):
        form = super(CampaignUpdate, self).get_form(form_class)
        return form

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['form_title'] = 'Editacia kampane'
        return context

    def form_valid(self, form):
        self.object = form.save()    
        return redirect(self.success_url) 

class CampaignDelete(DeleteView):
    model = Campaign
    template_name = "campaign/campaign_confirm_delete.html"

    def get_object(self, queryset=None):
        obj = super(DeleteView, self).get_object()
        if not obj:
            raise Http404
        return obj


class CampaignAcceptDelete(BaseDeleteView):
    model = Campaign
    success_url = 'campaign_list'

    def get(self, request, *args, **kwargs):
        self.get_object().delete()
        return redirect(self.success_url)


class MembershipView(View):
    template_name = "membership/index.html"
    #form_class = MembershipOfCompaignForm

    def get(self, request, *args, **kwargs):
        form_title = 'Priradenie kontaktov ku kampani'
        campaign = Campaign.objects.get(pk=kwargs['pk'])
        memberships = MembershipOfCompaign.objects.filter(campaign=campaign)
        persons =  Person.objects.all()
        return render(request, self.template_name, {'form_title': form_title,
            'campaign': campaign,
            'memberships': memberships,
            'persons': persons})

    def post(self, request, *args, **kwargs):
       
        campaign_id = request.POST['campaing_id']
        person_list = request.POST.getlist('person')

        if person_list is not None and campaign_id:
            campaign = Campaign.objects.get(pk=campaign_id)
            oldMembership = MembershipOfCompaign.objects.filter(campaign = campaign)
            for oldMember in oldMembership:
                oldMember.delete()
            for person_id in person_list:
                person = Person.objects.get(pk=person_id)
                isMember = MembershipOfCompaign.objects.filter(campaign = campaign, member=person)
                if not isMember:
                    newMember = MembershipOfCompaign(campaign = campaign, member=person)
                    newMember.save()

        return redirect('campaign_list')


def sendMessage(email, subject, html, solution):
    html = html.replace('{oslovenie}', solution)
    sender = Sender(user='morienstudio@gmail.com', password='qwerREWQ')
    sender.to = [email]
    sender.owner = "morienstudio@gmail.com"
    sender.subject = subject
    sender.html = html
    print html
    #sender.send()

class CampaignLunchView(TemplateView):
    template_name = "campaign/campaign_lunch.html"
    success_url = 'campaign_list'

    def get(self, request, *args, **kwargs):
        campaign = Campaign.objects.get(pk=kwargs['pk'])
        
        memberships = MembershipOfCompaign.objects.filter(campaign = campaign)

        for membership in memberships:
            sendMessage(membership.member.email, campaign.data_title, campaign.data_html, membership.member.solution)
            time.sleep(1)

        campaign.status = Campaign.STATUS[1][1]
        campaign.release_date = datetime.now()
        campaign.save()
        return redirect(self.success_url)

