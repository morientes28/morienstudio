from django.db import models
from django import forms
from django.forms import ModelForm, Textarea, IntegerField
from django.utils.translation import ugettext_lazy as _
from datetime import datetime

# ----- Models -------------------------------------------
class Segment(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

        
class Person(models.Model):
    GENDER = (
        ('M', 'Man'),
        ('F', 'Woman'),
    )
    first_name = models.CharField(max_length=32, default='', blank=True)
    last_name = models.CharField(max_length=32, default='', blank=True)
    email = models.EmailField(max_length=64)
    gender = models.CharField(max_length=1, default='M', choices=GENDER)
    solution = models.CharField(max_length=32, default='', blank=True)
    flag = models.CharField(max_length=32, default='', blank=True)
    segment = models.ForeignKey(Segment)

    def __str__(self):
        return self.email


class Campaign(models.Model):
    STATUS = (
        (0, 'Draft'),
        (1, 'Lunched'),
    )
    name = models.CharField(max_length=64)
    status = models.CharField(max_length=12, default=STATUS[0][1])
    release_date = models.DateTimeField(null=True, blank=True)
    create_date = models.DateTimeField(default=datetime.now())
    identify_flag = models.CharField(max_length=32, default='', blank=True)
    data_html = models.TextField(max_length=2048, default='{oslovenie}')
    data_title = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class MembershipOfCompaign(models.Model):
    campaign = models.ForeignKey(Campaign)
    member = models.ForeignKey(Person)

# ------ ModelForms -----------------------------------------

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = ['first_name', 'last_name', 'email', 'gender', 'solution', 'flag']
        labels = {
            'first_name': _('Nazov kampane'),
            'last_name': _('Stav'),
            'email': _('Email'),
            'gender': _('Pohlavie'),
            'solution': _('Segmentacia'),
            'flag': _('Priznak'),
        }

class CampaignForm(ModelForm):

    class Meta:
        model = Campaign
        widgets = {
            'data_html': forms.Textarea(attrs={'cols': 80, 'rows': 40}),
        }
        fields = ['name', 'status', 'release_date', 'identify_flag', 'data_title', 'data_html']
        labels = {
            'name': _('Nazov kampane'),
            'status': _('Stav'),
            'release_date': _('Datum spustenia'),
            'identify_flag': _('Identifikacia'),
            'data_title': _('Titulok'),
            'data_html': _('Text kampane'),
        }
        error_messages = {
            'name': {
                'max_length': _("Pocet znakov nazvu kampane prekrocila maximalnu hodnotu."),
            },
        }


class MembershipOfCompaignForm(forms.Form):


    def __init__(self, *args, **kwargs):
        super(MembershipOfCompaignForm, self).__init__(*args, **kwargs)
        for item in Person.objects.all():
            self.fields['person_%s' % item.id] = IntegerField()
        campaign = forms.IntegerField()

    