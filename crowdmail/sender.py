#!/usr/bin/env python
""" Sender for email sending """

__author__ = "morien [michalkalman@gmail.com]"

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from HTMLParser import HTMLParser


class Sender():

    def __init__(self, smtp_url="smtp.gmail.com", port_number=587, user=None, password=None):
        self._smtp = smtp_url
        self._port = port_number
        self._owner = None
        self._to = []
        self._text = None
        self._html = None
        self._subject = None
        self._smtpserver = None
        self._user = user
        self._password = password

    @property
    def smtp(self):
        return self._smtp

    @smtp.setter
    def smtp(self, value):
        if not value:
            raise ValueError("'smtp' parameter has to be set")
        else:
            self._smtp = value

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, value):
        if not value:
            raise ValueError("'user' parameter has to be set")
        else:
            self._user = value

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        if not value:
            raise ValueError("'password' parameter has to be set")
        else:
            self._password = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @property
    def to(self):
        return self._to

    @to.setter
    def to(self, value):
        self._to = value

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, value):
        if value.__len__() == 0:
            raise ValueError("'to' parameter has to have minimum one email address")
        else:
            self._owner = value

    @property
    def html(self):
        return self._html

    @html.setter
    def html(self, value):
        self._html = value

    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value):
        self._subject = value

    def authorization(self, user, password):
        self._smtpserver.ehlo()
        self._smtpserver.starttls()
        self._smtpserver.ehlo
        self._smtpserver.login(user, password)

    def create_message(self):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = self.subject
        msg['From'] = self.owner
        msg['To'] = ', '.join(self.to)

        if not self.text:
            s = MLStripper()
            s.feed(self.html)
            self.text = s.get_data().strip()

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(self.text, 'plain')
        part2 = MIMEText(self.html, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)
        return msg

    def send(self):
        self._smtpserver = smtplib.SMTP(self.smtp, self.port)
        self.authorization(self.user, self.password)
        msg = self.create_message()
        self._smtpserver.sendmail(msg['From'], msg['To'], msg.as_string())
        self._smtpserver.quit()

# --------------------------------------------------------------------------
# Class for cleaning tags from html string


class MLStripper(HTMLParser):

    def __init__(self):
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def test_sender():
    sender = Sender(user='morienstudio@gmail.com', password='qwerREWQ')
    sender.to = ['michalkalman@gmail.com']
    sender.owner = "morienstudio@gmail.com"
    sender.subject = "nieco"
    sender.text = "Ahoj!\nToto je testovaci email\nA tu je link:\nhttp://www.python.org"
    sender.html = """\
    <html>
      <head></head>
      <body>
        <h1>test</h1>
        <p>Ahoj!<br>
           <b>Ako sa mas?</b><br>
           Tu je link <a href="http://www.python.org">link</a> ak chces.
        </p>
      </body>
    </html>
    """
    sender.send()

if __name__ == "__main__":
    test_sender()
